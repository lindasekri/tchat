<?php

class Utilisateurs{
    private $_id;
    private $_nom;
    private $_prenom;
    private $_email;
    private $mdp;
    private $_gestionnaire = "tchat";
    private $_message;

    private function __construct(Array $donnees){
        $this->hydrate($donnees);

    }
    public function hydrate(Array $donnees){

        foreach($donnee as $key => $value){

            $methode = "set".ucfirst($key);
            
            if(method_exists($this, $methode)){
                $this->$methode($value);
            }
        }
            
    }

    public function getId(){
        return this-> _id;

    }
    public function getNom(){
        return this-> _nom;

    }
    public function getPrenom(){
        return this-> _prenom;

    }
    public function getEmail(){
        return this-> _email;

    }
    public function getMdp(){
        return this-> _mdp;

    }

    public function getMessage(){
        return this-> _message;

    }

    private function setId(int $id=NULL){
        if($id===NULL){
            $this->_id = self::$_decompte; 
        }else{
            $this->_id = $id;
        }
        self::$_decompte++;

    }
   
    private function setNom(string $nom){
        $this->_nom = $nom;
    }
    private function setPrenom(string $prenom){
        $this->_prenom = $prenom;

    }
    private function setEmail(string $email){
        $this->_email = $email;

    }
    private function setMdp(string $mdp){
        $this->_mdp = $mdp;

    }
    private function setMessage(string $message){
        $this->_message = $message;

    }
}