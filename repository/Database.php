<?php

/**
 * Classe permettant la connexion à la BDD
 */
class Database {
  // Définir les attributs
  const DB_HOST = "localhost";
  const DB_BASE = "tchat";
  const DB_USER = "tchat";
  const DB_MDP = "tchat";
  private $_BDD;

  public function __construct(){
    $this->connectBDD();
  }

  /**
   * Permet d'initialiser la connexion à la Base de Données
   */
  private function connectBDD(){
    try {
      $this->_BDD = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_MDP, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch (PDOException $e) {
      die("Erreur de connexion : ". $e->getMessage());
    }
      
  }
  public function getPDO(){
    return $this->PDO;
  } 
}
