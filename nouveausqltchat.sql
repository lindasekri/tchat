#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: utilisateurs
#------------------------------------------------------------

CREATE TABLE utilisateurs(
        id           Int  Auto_increment  NOT NULL ,
        nom          Varchar (50) NOT NULL ,
        prenom       Varchar (50) NOT NULL ,
        email        Varchar (50) NOT NULL ,
        mdp          Varchar (50) NOT NULL ,
        gestionnaire Bool NOT NULL
	,CONSTRAINT utilisateurs_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Message
#------------------------------------------------------------

CREATE TABLE Message(
        id_utilisateurs      Int NOT NULL ,
        id                   Int NOT NULL ,
        auteur               Varchar (50) NOT NULL ,
        text                 Longtext NOT NULL ,
        date                 Date NOT NULL ,
        statut               Bool NOT NULL ,
        id_utilisateurs_lire Int NOT NULL
	,CONSTRAINT Message_PK PRIMARY KEY (id_utilisateurs,id)

	,CONSTRAINT Message_utilisateurs_FK FOREIGN KEY (id_utilisateurs) REFERENCES utilisateurs(id)
	,CONSTRAINT Message_utilisateurs0_FK FOREIGN KEY (id_utilisateurs_lire) REFERENCES utilisateurs(id)
)ENGINE=InnoDB;

